

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import GitHubIcon from '@material-ui/icons/GitHub';


ReactDOM.render(
  <App
    message="Gif Generator"
    title="Search any Gif that you want"
    property="Designed by"
  />,
  document.getElementById('app')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

// Cristian Bustos"+ <GitHubIcon />  +"@galvisdeveloper - 2020 ©