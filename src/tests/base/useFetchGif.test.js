// import '@testing-library/jest-dom';
import React from 'react';
import GifExpertApp from '../../components/GifExpertApp';
import { shallow } from 'enzyme';

describe('Probando GifExpertApp', () => {

    let valor;
    let wrapper = shallow(<GifExpertApp title="Hola" />);

    beforeEach(() => {
        valor = 100;
        wrapper = shallow(<GifExpertApp value={valor} />);
    });

    test('Debe de mostrar <GifExpertApp /> Correctamente', () => {

        expect(wrapper).toMatchSnapshot();

    });


    test('Debe de mostrar el valor por defecto de Search any Gif that you want', () => {

        const valorH2 = wrapper.find('h2').text();
        expect(parseInt(valorH2)).toBe(valor);

    });
    
});